<?php get_header(); ?>

		<div class="container_12" id="main" style="padding:0px 0 61px;">
			<?php
				$link= new Pod('startimage');
				$link->findRecords('sort_order ASC');
				$total_links = $link->getTotalRows();
$counter = 0;

			?>
			<div id="loopedSlider" style="clear:both; margin:0 auto;position:relative;width:950;">
				<div class="container" style="cursor:pointer;height:369px;overflow:hidden;position:relative;width:950px;">
					<div class="slides" style="position:absolute;width:2850px;top:0px;left:0px;">

<?php while ( $link->fetchRecord() ) :
				$title = $link->get_field('name');
				$image = $link->get_field('image');
				$url= $link->get_field('url');

if($total_links > 1 && $counter == $total_links -1  ) $counter = -1;
?>
						<div style="position: absolute; left: <?php echo $counter * 950?>px; display: block;">
							<a href="<?php echo $url?>"><img width="950" height="369" src="<?php echo $image[0]['guid']?>"></a>
						</div>
<?php
$counter = $counter+1;
endwhile ?>

					</div>
				</div>
			</div>

			<!--<img src="kk_start_main.png">-->
			<div class="grid_16" id="content">
				<div class="grid_6 alpha" style="width: 440px;margin:0 0 0 40px;">
					<div class="grid_6 alpha" id="visual" style="background:none;width: 440px;padding:70px 0 15px;margin:0;border-bottom: 1px solid #e8e8e8">
						<h1 style="margin:0 0 12px 140px;" class="title-horna">Baristans hörna</h1>
<?php $home_page_id = get_option('woo_home_page_id'); ?>
<?php if ($home_page_id && ($home_page_id <> "Select a page:")) : ?>

<?php query_posts('page_id=' . get_page_id( $home_page_id )); ?>
<?php if (have_posts()) : the_post(); ?>
<p style="margin:0 0px 0 140px;"><?php the_content(); ?></p>
<?php endif; ?>
<?php endif; ?>

						<img src="<?php bloginfo('template_directory'); ?>/images/barista_big.gif" width="123" height="190" alt="" class="pic" style="bottom:15px;left:0px;" />
					</div>
					<?php wp_reset_query(); ?>
					<?php if (have_posts()) : $count = 0; ?>
					<?php while (have_posts() && $count < 2) : the_post(); $count++; ?>
					<div class="grid_6 alpha" style="font:11px Arial,Verdana,Helvetica,sans-serif;width:420px;padding-left:20px;margin-top:20px;padding-bottom:20px;border-bottom:1px solid #e8e8e8;">
						<span style="color:#676767;float:left;margin-top:12px;margin:0;line-height:14px;"><?php the_time('d M, Y'); ?> -&nbsp;</span><h3 style="line-height:14px;font-size:12px;margin:0;"><?php the_title(); ?></h3>
						<p style="line-height:14px;color:#676767;margin:2px 0 2px 0;"><?php the_excerpt(); ?></p>
						<a href="<?php the_permalink() ?>" style="color:#e53138;">Les mer</a>
					</div>
					<?php endwhile;?>
					<?php endif; ?>
				</div>

				<div class="grid_6 omega" id="sidebar" style="width: 420px;margin-left:40px;margin-top:48px;font:13px Arial,Verdana,Helvetica,sans-serif;">
					<div>

			<?php
				$splash= new Pod('startsplash');
				$splash->findRecords();

				$splash->fetchRecord();

				$title = $splash->get_field('name');
$content = $splash->get_field('content');
				$image = $splash->get_field('image');
				$url= $splash->get_field('url');

			?>

						<img src="<?php echo $image[0]['guid']; ?>" style="margin-bottom:32px;">
						<h3><?php echo $title; ?></h3>
						<p style="color:#676767;"><?php echo $content; ?></p>
						<a class="more" href="<?php echo $url; ?>">Las mer</a>
					</div>
				</div>
			</div>

<?php get_footer(); ?>
