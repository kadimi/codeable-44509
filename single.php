<?php get_header(); ?>

		<div class="container_12" id="main">
			<div class="grid_8" id="content">
				<!--
				<div class="grid_7 alpha" id="visual">
					<h1 class="title-horna"><a alt="Barista Corner" href="http://www.kaffeknappen.se/barista_corner" class="t-script t-large">Barista Corner</a></h1>
					<p>Våra egna, utbildade och meriterade baristor bjuder här på kunskaper, nyheter, trender och allt annat som är värt att veta om kaffe. Helt enkelt en riktig inspirationskälla för alla kaffeälskare! Det är alltså här du ska hänga om du vill vara först med det senaste och alltid vara uppdaterad på den ädla kaffekonsten.</p>
					<img src="<?php bloginfo('template_directory'); ?>/images/barista.gif" width="91" height="144" alt="" class="pic" />
				</div>
				-->
				<div class="grid_8 area mb-xlarge">
					<img src="<?php echo get_bloginfo('template_directory')?>/images/barista-logo-large.png" alt="">
				</div>
				
				<?php if (have_posts()) : $count = 0; ?>
					<?php while (have_posts()) : the_post(); $count++; ?>

				<div class="clear"></div>
				<div class="grid_8 area">
					<div class="barista-date">
						<strong><?php the_time('H:i'); ?></strong>
						<strong><?php the_time('d M'); ?></strong>
						<?php the_time('Y'); ?>
					</div>
					<div class="blog-text text">
						<div class="blog-title">
						<h1><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
						</div>
						<?php the_content(); ?>
					</div>
				</div>

					<?php endwhile; else: ?>
					<div class="post">
                		<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
					</div><!-- /.post -->
				<?php endif; ?>
			</div>
			<div id="sidebar">
			<div class="barista-team-sidebar">
				<a class="speciality-coffee" href="http://scae.com"><img src="<?php echo get_bloginfo('template_directory')?>/images/icons/speciality-coffee.png" alt=""></a>
				<h2>Det er vi som er <br>baristateamet</h2>
				<img src="<?php echo get_bloginfo('template_directory')?>/images/barista-team.jpg" alt="">
				<p>Antallet sertifiserte baristaer hos Kaffeknappen nærmer seg 100! Det vårt team ikke vet om kaffe, er det ikke verdt å vite. Vi elsker å kjenne på pulsen på de siste kaffetrenene. I vår blogg Barista Corner deler vi nyheter, kunnskap, tips og mye annet som en kaffeelsker setter pris på. </p> 
				<p>Faktisk er alle ansatte i Kaffeknappen utdannet og <a href="http://www.kaffeknappen.se/om-kaffeknappen/100-certifierade">sertifisert i henhold til  SCAE’s utdannelsessystem,</a> den fremste og mest respekterte organisasjonen i verden når det kommer til kaffekunnskaper. Så om du har spørsmål om kaffe, kan du spørre hvem som helst i Kaffeknappen!</p>
			</div>

			<?php 
				$barista_title = get_post_meta(6, 'barista_title', true);
				$barista_img = get_post_meta(6, 'barista_image' ,true);
				$barista_text = get_post_meta(6, 'barista_text', true);
			?>
			<?php if($barista_title && $barista_title != '') { ?>
				<div class="barista-monthly-sidebar">
					<h2><?php echo $barista_title; ?></h2>
					<?php if($barista_img) {?>
						<img src="<?php echo $barista_img[0]['image']; ?>" alt="">
					<?php } ?>
					<div>
						<?php echo $barista_text; ?>
					</div>
				</div>
			<?php } ?>
			<?php dynamic_sidebar('sidebar-1'); ?>
			</div>
		</div>
<?php get_footer(); ?>