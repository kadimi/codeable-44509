<?php
/*
Template Name: Events / Coming up
*/
?>

<?php get_header(); ?>

		<div class="container_12" id="main">
			<div class="grid_8" id="content">
				<?php if (have_posts()) : $count = 0; ?>
					<?php while (have_posts()) : the_post(); $count++; ?>
<?php the_content(); ?>
                                        <?php endwhile;?>
                                <?php endif; ?>
				<?php $args = array( 'paged'=> $paged, 'cat' => 15 ); query_posts($args); ?>
				<?php if (have_posts()) : $count = 0; ?>
					<?php while (have_posts()) : the_post(); $count++; ?>

				<div class="clear"></div>
				<div class="grid_8 area">
					<div class="box">
						<strong><?php the_time('H:i'); ?></strong>
						<strong><?php the_time('d M'); ?></strong>
						<?php the_time('Y'); ?>
					</div>
					<div class="text">
						<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
<?php the_content(); ?>
					</div>
				</div>

					<?php endwhile; else: ?>
					<div class="post">
                		<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
					</div><!-- /.post -->
				<?php endif; ?>
			</div>
			<div id="sidebar">
<?php dynamic_sidebar('sidebar-1'); ?>
			</div>
		</div>
<?php get_footer(); ?>
