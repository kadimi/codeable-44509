<?php
/*
Template Name: Product-single
*/
?>



<?php get_header(); ?>

		<!-- PAGE START -->
		<div class="product-container" id="main">
			<div class="grid_3 alpha product-list hide-lg">
				<?php
				 $args = array(
					'sort_order' => 'ASC',
					'sort_column' => 'menu_order',
					'child_of' => $post->post_parent
				);
					$parents = get_pages($args);
					 	if (!empty($parents)) {
					       	foreach($parents as $parent) {
					       	$thumbnail = get_post_meta($parent->ID, 'product_thumbnail', true);
				       		if 	(!empty($thumbnail)) {
					            $title = get_the_title($parent->ID);
					            $slug = get_permalink($parent->ID);
					            echo '<div class="product">';
					           	echo '<a href="'.$slug.'"><img src="'.$thumbnail[0]['image'].'" alt="'.esc_attr($thumbnail[0]['title']).'" width="120" height="95" />';
					            echo '<p>'.$title.'</p></a>';
					            echo '</div>';
					        }
					    }
					}
				?>
			</div>
			<div class="grid_9 product-content b-left-dotted" id="content">
		        <?php
		            $image = get_post_meta($post->ID, 'product_image', true);
		            $details = get_post_meta($post ->ID, 'product_details', true);
		            $pdf = get_post_meta($post ->ID, 'product_pdf', true);
					if (!empty($details)) { ?>
			        <div class="product-info">
				            <div class="product-image">
					            <?php echo '<img src="'.$image[0]['image'].'" alt="'.esc_attr($image[0]['title']).'" width="490" height="400" />'; ?>			
					        </div>
					        <div class="product-details">
					        	<div class="product-spec"><?php echo $details; ?></div>
					        	<?php if(!empty($pdf)) { ?>
					        	<div class="pdf-box">
						        	<span>
								        <?php echo '<a href="'.$pdf.'">Last ned PDF'; ?>
								        <img src="<?php bloginfo('template_directory'); ?>/images/pdf-icon.png" target="_blank" alt="pdf" width="40" height="40" /></a>
							    	</span>
						    	</div>
						    	<?php } ?>
				            </div>
			        	</div>
			        	<div class="product-text">
		            <?php } ?>
		             <?php if (empty($details)) { ?>
			            <div class="product-page">
		            <?php } ?>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>


							<?php $parent_link = get_permalink($post->post_parent); ?>
							<div class="product-btn">
								<?php if(!empty($pdf)) { ?>
									<?php echo '<a href="'.$pdf.'">Last ned PDF'; ?>
								    <img src="<?php bloginfo('template_directory'); ?>/images/pdf-icon.png" target="_blank" alt="pdf" width="30" height="30" /></a>
								<?php } ?>
								<a href="<?php echo $parent_link; ?>">Oppdage mer av våre produkter</a>
							</div>
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>

					<div class="contact-sidebar">
					<img src="<?php echo get_bloginfo('template_directory')?>/images/contact-person.png" alt="">
					<h2>Vil du at <span>vi kontakter deg?</span></h2>
					<p>Fyll ut opplysningene nedenfor, så hører du fra oss.</p>
					<?php echo do_shortcode( '[contact-form-7 id="3274" title="Kontaktformulär 1"]' ); ?>
					</div>
					


				</div>
				<div class="clear"></div>
			</div>

		</div>



		<!-- PAGE END -->

<?php get_footer(); ?>
