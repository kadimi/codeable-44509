<?php
/*
Template Name: Products
*/
?>



<?php get_header(); ?>
		
		<!-- PAGE START -->
		<div class="product-container" id="main">
			<div class="grid_3 alpha product-list">
				<?php
				 $args = array(
					'sort_order' => 'ASC',
					'sort_column' => 'menu_order',
					'child_of' => $post->ID
				);
				    $children = get_pages($args);
				    if (!empty($children)) {
				        foreach($children as $child) {
				        	$thumbnail = get_post_meta($child->ID, 'product_thumbnail', true);
				        	if 	(!empty($thumbnail)) {
				     			$title = get_the_title($child->ID);
				            	$slug = get_permalink($child->ID);
					            echo '<div class="product">';
					           	echo '<a href="'.$slug.'"><img src="'.$thumbnail[0]['image'].'" alt="'.esc_attr($thumbnail[0]['title']).'" width="120" height="95" />';
					            echo '<p>'.$title.'</p></a>';
					            echo '</div>';
				        	}
				        }
				    }
				?>
			</div>
			<div class="grid_9 product-content b-left-dotted" id="content">
				<div class="product-page hide-md">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
					<?php endif; ?>
				</div>
				<div class="clear"></div>

				<div class="contact-sidebar contact-sidebar-products" style="margin-left:14px !important;">
					<img src="<?php echo get_bloginfo('template_directory')?>/images/contact-person.png" alt="">
					<h2>Vil du at <span>vi kontakter deg?</span></h2>
					<p>Fyll ut opplysningene nedenfor, så hører du fra oss.</p>
					<?php echo do_shortcode( '[contact-form-7 id="3274" title="Kontaktformulär 1"]' ); ?>
				</div>


			</div>


		</div>

				

		<!-- PAGE END -->

<?php get_footer(); ?>
