<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<?php woo_meta();
	if(is_home()) { ?>
		<meta name="description" content="Kaffeknappen er eksperter på kaffeløsninger for bedrifter. Det siste innen kaffemaskiner, kaffekverner, kaffebønner, kaffe og espresso, det hele krydret med våre baristas' spesialistkunnskaper – med Kaffeknappen som leverandør får du full oversikt over alt som har med kaffe å gjøre." />
		<meta name="keywords" content="kaffe, kaffeknappen , kunnskap om kaffe, kaffe , kaffeløsninger, møte eksperten" />
	<?php } ?>
	<meta name="google-site-verification" content="I4--OY43vATKAHa2Nxo35hQyiXhvNCMsQxsFQr90Rtw" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(''); if(!is_home()){ echo ' | ';} bloginfo('name'); ?> </title>
	<link href="<?php bloginfo('template_directory'); ?>/css/all.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php bloginfo('template_directory'); ?>/css/page.css" rel="stylesheet" type="text/css" media="all" />

	<link href="<?php bloginfo('template_directory'); ?>/css/960.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php bloginfo('template_directory'); ?>/css/responsive.css" rel="stylesheet" type="text/css" media="all" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/lt7.css" /><![endif]-->

	<script  type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script  type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/includes/js/jquery.easing.min.js' ?>"></script>
	<script  type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/includes/js/loopedslider.min.js' ?>"></script>
	<script  type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/includes/js/jquery.cycle.all.latest.js' ?>"></script>
	<script  type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/includes/js/jquery.placeholder.min.js' ?>"></script>

	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory').'/fancybox/source/jquery.fancybox.css?v=2.1.4' ?>" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/fancybox/source/jquery.fancybox.pack.js?v=2.1.4' ?>"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory').'/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5' ?>" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5' ?>"></script>
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.5' ?>"></script>

	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory').'/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7' ?>" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo get_bloginfo('template_directory').'/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7' ?>"></script>

	<!--[if IE 7]>
		<style>
			.fancybox-title p {
				margin-top: 20px !important;
			}
		</style>
	<![endif]-->


	<script charset="utf-8" type="text/javascript">

		$(document).ready(function() {
		<?php
			if(is_home()){
		?>
				$('#fadeSlider').cycle({
					fx: 'fade',
					timeout: 6000
				});

				$('#loopedSlider').loopedSlider({
					autoStart: 4000,
					containerClick:false,
					slidespeed:1500,
					fadespeed: 5000
				});
		<?php
			}
		?>
			$('.fancybox').fancybox();
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,
				helpers : {
					title : {
						type : 'inside'
					}
				}
			});
		
			$("form .wpcf7-text").each(function() {
		    	var value = $(this).attr("value");
		    	$(this).attr("value", '');   
		        $(this).attr("placeholder", value);
		    });
		    $('input, textarea').placeholder();
		    var navItems = $('.mobile-nav ul > li a').not('ul li ul a');
		    var subMenus = $('.mobile-nav .sub-menu');
		    navItems.each(function() {
		    	var subMenu = $(this).siblings('.sub-menu');
		    	$(this).on('click', function(e) {
		    		e.preventDefault();
		    		if(subMenu.length === 0) {
		    			location.href = $(this).attr('href');
		    		}
		    		if(subMenu.hasClass('open')) {
		    			subMenu.removeClass('open');
		    			subMenu.slideUp();
		    		} else {
		    			subMenus.each(function() {
		    				if($(this).hasClass('open')) {
		    					$(this).removeClass('open');
		    					$(this).slideUp();
		    				}
		    			});
		    			subMenu.addClass('open');
		    			subMenu.slideDown();
		    		}

		    	})
		    })
		});
	</script>
	<!--[if gte IE 9]>
  		<style type="text/css">
    		.gradient {
       			filter: none;
    		}
 	 	</style>
	<![endif]-->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P3QVWB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P3QVWB');</script>
<!-- End Google Tag Manager -->

</head>
	<body  <?php body_class();?>
		<?php
		global $post;
		if(is_front_page()) {
			$bg = get_post_meta(3, 'background_image', true);
			$background_name = get_post_meta(3, 'background_name', true);
		}
		elseif ( is_single() || is_archive() ){
			$bg = get_post_meta(6, 'background_image', true);
			$background_name = get_post_meta(6, 'background_name', true);
		}
		else {
			$bg = get_post_meta($post->ID, 'background_image', true);
			$background_name = get_post_meta($post->ID, 'background_name', true);
			if(empty($bg)){
				$parentBg = $post->post_parent;
				$parentBg2 = $wpdb->get_var("SELECT post_parent FROM $wpdb->posts WHERE ID=$parentBg");
				$bg = get_post_meta($parentBg, 'background_image', true);
				$background_name = get_post_meta($parentBg, 'background_name', true);
				if(empty($bg)){
					$bg = get_post_meta($parentBg2, 'background_image', true);
					$background_name = get_post_meta($parentBg2, 'background_name', true);
				}
			}
		}
		if 	(!empty($bg)) {
			echo 'style="background: url(' . $bg[0]['image'] . ') fixed no-repeat #e6e6e6; 	background-size: 100%; -webkit-background-size: 100%; -moz-background-size: 100%;">';
		} else {
			echo '>';
		}
		?>
	<div id="wrapper">
	<?php if(!empty($background_name) && !empty($bg)) { ?>

			<?php
				$bgSet = false;
				$activeImg = pods('bakgrundsbilder');
				$activeImg->find();
				while ( $activeImg->fetch()) :
					$bgImage = $activeImg->field('image');
					$description = $activeImg->field('description');
					$bgName = $activeImg->field('name');
				if($bgName === $background_name) {
					$bgSet = true;
					?>
					<a href="<?php echo $bgImage[0]['guid'] ?>" title="<?php echo strip_tags($description) ?>" class="fancybox-effects-c" rel="group">
						<div class="om-bilden">
							<img src="<?php bloginfo('template_directory'); ?>/images/knapp_no.png" alt="" />
						</div>
					</a>
				<?php }
				endwhile;
				if($bgSet) {
					$slideImages = pods('bakgrundsbilder');
					$slideImages->find();
					while ( $slideImages->fetch()) :
						$slideImg = $slideImages->field('image');
						$slideDescription = $slideImages->field('description');
						$slideName = $slideImages->field('name');
					if($slideName !== $background_name){ ?>
						<div class="fancybox-hidden">
							<a href="<?php echo $slideImg[0]['guid'] ?>" title="<?php echo strip_tags($slideDescription) ?>" class="fancybox-effects-c" rel="group"></a>
						</div>
					<?php }
					endwhile;
				} ?>
	<?php } ?>
		<div id="header">
			<div class="main-nav">
				<a href="<?php bloginfo('url'); ?>" class="logo"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="" /></a>
				<ul class="sub-nav" style="height:9px">
					<li><a href="http://www.kaffeknappen.se">Svenska</a></li>
					<li class="active"><a href="http://www.kaffeknappen.no">Norsk</a></li>
				</ul>
				<ul class="nav">
				<?php
					if (get_option('woo_custom_nav_menu') == 'true') {
						if (function_exists('woo_custom_navigation_output'))
							woo_custom_navigation_output();
					} else {
						$exclude = get_option('woo_nav_exclude'); if ( get_option('woo_ex_feat_pages') == "true" ) $exclude .= ',' . get_option('woo_feat_pages');
						if ( is_page() ) $highlight = "page_item"; else $highlight = "page_item current_page_item";
				?>
						<li class="<?php echo $highlight; ?>"><a href="<?php bloginfo('url'); ?>"><?php _e('Home', 'woothemes') ?></a></li>
	            <?php
						if (get_option('woo_cat_menu') == 'true')
							wp_list_categories('sort_column=menu_order&depth=6&title_li=&exclude='.$exclude);
						else
							wp_list_pages('sort_column=menu_order&depth=6&title_li=&exclude='.$exclude);
					}
				?>
				</ul>
				<?php
					$thepageid;
					wp_cache_delete($post->ID, 'posts');
					$ancestors = get_post_ancestors($post->ID);

					if($post->post_parent > 0) {

					$count = end($ancestors);
					$thepageid = end($ancestors);
					reset($ancestors);

					} else{

						$thepageid = $post->ID;

					}

					$pageset = get_posts('post_type=page&numberposts=-1&orderby=menu_order&post_parent='.$thepageid );
					$numItems = count($pageset);
				?>
				<ul class="child-nav">
				<?php
					if($numItems>0) {
						$i = 1;
						foreach ($pageset as $apage) {
							$linkstatus='';

							if((array_search($apage->ID,$ancestors) >-1) || ($post->ID == $apage->ID)){
								$linkstatus='active';
							}

							echo '<li class="' . $linkstatus . '"><a href="'.get_permalink($apage).'">'.$apage->post_title.'</a></li>';
							$i++;
						}
					}
				?>
				</ul>

			</div>
			<div class="mobile-nav">
				<?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'container_class' => 'menu-header', 'depth' => 2, ) ); ?>
			</div>
			<div class="clear"></div>
		</div>
