<?php get_header(); ?>
       
    <div id="content" class="col-full">
		<div id="main" class="col-left box">
            
		<?php if (have_posts()) : $count = 0; ?>
        
            <span class="archive_header"><?php _e('Search results', 'woothemes') ?> for <?php printf(__('\'%s\''), $s) ?></span>
            
        <?php while (have_posts()) : the_post(); $count++; ?>
                                                                    
            <div class="post">

                <h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                
                <p class="post-meta">
                    <span class="date"><?php the_time($GLOBALS['woodate']); ?></span>
                    <span><?php _e('Categories', 'woothemes') ?>: <?php the_category(', ') ?></span>
                    <span><?php _e('by', 'woothemes') ?>  <?php the_author_posts_link(); ?></span>
                    <span class="comments"><?php comments_popup_link(__('0 Comments', 'woothemes'), __('1 Comment', 'woothemes'), __('% Comments', 'woothemes')); ?></span>
                </p>
                
                <div class="entry">
                    <?php the_excerpt(); ?>
                </div><!-- /.entry -->

            </div><!-- /.post -->
                                                
	   <?php endwhile; else: ?>
            <div class="post">
                <p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
           </div><!-- /.post -->
       <?php endif; ?>  
    
			<?php woo_pagenav(); ?>                
        
        </div><!-- /#main -->

        <?php get_sidebar(); ?>

    </div><!-- /#content -->
		
<?php get_footer(); ?>
