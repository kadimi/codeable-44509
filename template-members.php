<?php
/*
Template Name: Medlemmar (lista)
*/

?>
<?php get_header(); ?>
       
    <div id="content" class="col-full">
		<div id="main" class="col-left box">
		
			<?php if ( get_option( 'woo_breadcrumbs' ) == 'true') { yoast_breadcrumb('<div id="breadcrumb"><p>','</p></div>'); } ?>
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div <?php post_class(); ?>>

                    <h1 class="title"><?php the_title(); ?></h1>
                    
                    <div class="entry">
	                	<?php the_content(); ?>


        <?php
          $link= pods('members');
          $link->find('name ASC');
          $total_links = $link->total_found();
        ?>

	<?php if( $total_links >0 ) : ?>
		<table class="links">
<thead>
				<tr>
					<th width="50%">Organisation</th>
					<th width="50%">Länk</th>
				</tr>
</thead>
			<tbody>

			<?php while ( $link->fetch() ) : ?>
				<?php
					// set our variables
					$member_title = $link->field('name');
					$member_url= $link->field('url');			
				?>
				<tr>
					<td><?php echo $member_title; ?></td>
					<td><a href="<?php echo $member_url; ?>" target="_new"><?php echo $member_url; ?></a></td>

				</tr>
			<?php endwhile ?>
			</tbody>
		</table>
    <?php endif ?>



	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                
                <?php if ('open' == $post->comment_status) : ?>
	                <?php comments_template(); ?>
				<?php endif; ?>
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->

        <?php get_sidebar('page'); ?>

    </div><!-- /#content -->
		
<?php get_footer(); ?>