<?php 

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Ajax Handlers
	- Tumblog

-----------------------------------------------------------------------------------*/

require_once("../../../../wp-config.php");

/*-----------------------------------------------------------------------------------*/
/* Tumblog */
/*-----------------------------------------------------------------------------------*/

$tumblog_custom_fields = array(	'video-embed' => 'video-embed',
								'quote-copy' => 'quote-copy',
								'quote-author' => 'quote-author',
								'quote-url' => 'quote-url',
								'link-url' => 'link-url'
								);
								
if ($_POST['tumblog-type'] == 'article') 
{
	
	$data = $_POST;
	$type = 'note';
	
	tumblog_publish($type, $data);
	die ( 'OK' );
}

elseif ($_POST['tumblog-type'] == 'video') 
{
	
	$data = $_POST;
	$type = 'video';
	
	tumblog_publish($type, $data);
	die ( 'OK' );
}

elseif ($_POST['tumblog-type'] == 'image') 
{
	
	$data = $_POST;
	$type = 'image';
	
	tumblog_publish($type, $data);
	die ( 'OK' );
}

elseif ($_POST['tumblog-type'] == 'link') 
{
	
	$data = $_POST;
	$type = 'link';
	
	tumblog_publish($type, $data);
	die ( 'OK' );
}

elseif ($_POST['tumblog-type'] == 'quote') 
{
	
	$data = $_POST;
	$type = 'quote';
	
	tumblog_publish($type, $data);
	die ( 'OK' );
}

elseif ($_POST['tumblog-type'] == 'audio') 
{
	
	$data = $_POST;
	$type = 'audio';
	
	tumblog_publish($type, $data);
	die ( 'OK' );
}

elseif ($_POST['tumblog-type'] == 'chat') 
{
	
	$data = $_POST;
	$type = 'chat';
	
	tumblog_publish($type, $data);
	die ( 'OK' );
}

else {
	die ( 'OK' );
}

//Functionality
function tumblog_publish($type, $data) {
	global $current_user;
    get_currentuserinfo();
	$tumblog_custom_fields = array(	'video-embed' => 'video-embed',
								'quote-copy' => 'quote-copy',
								'quote-author' => 'quote-author',
								'quote-url' => 'quote-url',
								'link-url' => 'link-url',
								'image-url' => 'image',
								'audio-url' => 'audio'
								);
	switch ($type) 
	{
    	case 'note':
        	// Create post object
  			$tumbl_note = array();
  			$tumbl_note['post_title'] = $data['note-title'];
  			$tumbl_note['post_content'] = $data['tumblog-content'];
  			$tumbl_note['post_status'] = 'publish';
  			$tumbl_note['post_author'] = $current_user->ID;
  			
  			$category_id = get_cat_ID( get_option('woo_articles_category') );
  			$tumbl_note['post_category'] = array($category_id);

			// Insert the note into the database
  			$post_id = wp_insert_post($tumbl_note);

        	break;
    	case 'video':
    	    // Create post object
  			$tumbl_note = array();
  			$tumbl_note['post_title'] = $data['video-title'];
  			$tumbl_note['post_content'] = $data['tumblog-content'];
  			$tumbl_note['post_status'] = 'publish';
  			$tumbl_note['post_author'] = $current_user->ID;
  			
  			$category_id = get_cat_ID( get_option('woo_videos_category') );
  			$tumbl_note['post_category'] = array($category_id);
			
			// Insert the note into the database
  			$post_id = wp_insert_post($tumbl_note);
    	    add_post_meta($post_id, $tumblog_custom_fields['video-embed'], $data['video-embed'], true);
    	    
    	    break;
    	case 'image':
    	    // Create post object
  			$tumbl_note = array();
  			$tumbl_note['post_title'] = $data['image-title'];
  			$tumbl_note['post_content'] = $data['tumblog-content'];
  			$tumbl_note['post_status'] = 'publish';
  			$tumbl_note['post_author'] = $current_user->ID;
  			
  			$category_id = get_cat_ID( get_option('woo_images_category') );
  			$tumbl_note['post_category'] = array($category_id);

			// Insert the note into the database
  			$post_id = wp_insert_post($tumbl_note);
    	        	    
    	    if ($data['image-id'] > 0) {
    	    	$my_post = array();
    	    	$my_post['ID'] = $data['image-id'];
	 		 	$my_post['post_parent'] = $post_id;
				// Update the post into the database
  				wp_update_post( $my_post );
  				add_post_meta($post_id, $tumblog_custom_fields['image-url'], $data['image-upload'], true);
    	    }
    	    else {
    	    	add_post_meta($post_id, $tumblog_custom_fields['image-url'], $data['image-url'], true);
    	    }
  			    	    
    	    break;
    	case 'link':
    	    // Create post object
  			$tumbl_note = array();
  			$tumbl_note['post_title'] = $data['link-title'];
  			$tumbl_note['post_content'] = $data['tumblog-content'];
  			$tumbl_note['post_status'] = 'publish';
  			$tumbl_note['post_author'] = $current_user->ID;
  			
  			$category_id = get_cat_ID( get_option('woo_links_category') );
  			$tumbl_note['post_category'] = array($category_id);

			// Insert the note into the database
  			$post_id = wp_insert_post($tumbl_note);
  			add_post_meta($post_id, $tumblog_custom_fields['link-url'], $data['link-url'], true);
  			
    	    break;
    	case 'quote':
    	    // Create post object
  			$tumbl_note = array();
  			$tumbl_note['post_title'] = $data['quote-title'];
  			$tumbl_note['post_content'] = $data['tumblog-content'];
  			$tumbl_note['post_status'] = 'publish';
  			$tumbl_note['post_author'] = $current_user->ID;
  			
  			$category_id = get_cat_ID( get_option('woo_quotes_category') );
  			$tumbl_note['post_category'] = array($category_id);

			// Insert the note into the database
  			$post_id = wp_insert_post($tumbl_note);
    	    add_post_meta($post_id, $tumblog_custom_fields['quote-copy'], $data['quote-copy'], true);
    	    add_post_meta($post_id, $tumblog_custom_fields['quote-author'], $data['quote-author'], true);
    	    add_post_meta($post_id, $tumblog_custom_fields['quote-url'], $data['quote-url'], true);
    	    
    	    break;
    	case 'audio':
    	    // Create post object
  			$tumbl_note = array();
  			$tumbl_note['post_title'] = $data['audio-title'];
  			$tumbl_note['post_content'] = $data['tumblog-content'];
  			$tumbl_note['post_status'] = 'publish';
  			$tumbl_note['post_author'] = $current_user->ID;
  			
  			$category_id = get_cat_ID( get_option('woo_audio_category') );
  			$tumbl_note['post_category'] = array($category_id);

			// Insert the note into the database
  			$post_id = wp_insert_post($tumbl_note);
    	    
    	    if ($data['audio-id'] > 0) {
    	    	$my_post = array();
    	    	$my_post['ID'] = $data['audio-id'];
	 		 	$my_post['post_parent'] = $post_id;
				// Update the post into the database
  				wp_update_post( $my_post );
  				add_post_meta($post_id, $tumblog_custom_fields['audio-url'], $data['audio-upload'], true);
    	    }
    	    else {
    	    	add_post_meta($post_id, $tumblog_custom_fields['audio-url'], $data['audio-url'], true);
    	    }
    	    
    	    break;
    	case 'chat':
    	    // Create post object
  			$tumbl_note = array();
  			$tumbl_note['post_title'] = $data['chat-title'];
  			$tumbl_note['post_content'] = $data['tumblog-content'];
  			$tumbl_note['post_status'] = 'publish';
  			$tumbl_note['post_author'] = $current_user->ID;
  			
  			$category_id = get_cat_ID( get_option('woo_chat_category') );
  			$tumbl_note['post_category'] = array($category_id);

			// Insert the note into the database
  			$post_id = wp_insert_post($tumbl_note);
    	    
    	    break;
    	default:
    		break;
	}
	
}


?>