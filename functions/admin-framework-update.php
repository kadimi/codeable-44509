<?php

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- woothemes_framework_update_page
- woothemes_framework_update_head

-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* woothemes_more_themes_page */
/*-----------------------------------------------------------------------------------*/

function woothemes_framework_update_page(){
		$update_url = 'http://www.woothemes.com/u/test-file.zip';
        ?>
            <div class="wrap themes-page">
            <h2>Framework Update</h2>
            <form method="post"  enctype="multipart/form-data" id="wooform" action="<?php echo admin_url('admin.php?page=woothemes_framework_update'); ?>">
				<?php wp_nonce_field('update-options'); ?>
                <p>The aim of this updater is to update the "functions" folder of your theme with the latest framework archive from the WooThemes.com server.</p>
                <p><strong>This feature is still in beta mode</strong> so it's safe to use, and won't do any overwriting of any important files.
                    At this point in time it will only (is successful) add a "test-file.txt" to your theme's functions folder.
                    Test it out and see if it works.</p>
                
                <p>Latest framework: <code><?php echo $update_url; ?></code></p>
                
                <p>&rarr; <strong>Your version:</strong> <?php echo get_option('woo_framework_version'); ?></p>
                
                <p>&rarr; <strong>Current Version:</strong> <?php woo_get_fw_version(); ?></p>
                
                <input type="hidden" name="update_url" value="<?php echo $update_url; ?>" />
                <input type="submit" value="Update Framework" />
                <input type="hidden" name="woo_update_save" value="save" />
                
                
            </form>
            </div>
            <?php
};

function woo_get_fw_version(){

	$fw_url = 'http://www.woothemes.com/demo/wooframework/wp-content/themes/wooframework/functions/functions-changelog.txt';
	@$file_contents = file($fw_url);
	
	if($file_contents) {
		foreach ($file_contents as $line_num => $line) {
                            
				$current_line =  $line;
				
				if($line_num > 1){    // Not the first or second... dodgy :P
					
					if (preg_match('/^[0-9]/', $line)) {

							$current_line = strstr($current_line,"version");
							$current_line = preg_replace('~[^0-9,.]~','',$current_line);
							$output = $current_line;
							break;
					}
					
				
				} 	
	
		}
		echo $output;
		
	} else {
		echo 'Currently Unavailable';
	}

}

function woothemes_framework_update_head(){

	if(isset($_POST['woo_update_save'])){		
		if($_POST['woo_update_save'] == 'save'){
			if(isset($_POST['update_url'])){
				
				//Setup Filesystem 
				
				
				$filesystem = WP_Filesystem();	
				
				if($filesystem == false){
					
					function woothemes_framework_update_filesystem_warning() {
							$method = get_filesystem_method();
							echo "<div id='filesustem-warning' class='updated fade'><p>Failed: Filesystem preventing downloads. (". $method .")</p></div>";
						}
						add_action('admin_notices', 'woothemes_framework_update_filesystem_warning');
						return;
				}
				
				
				
				$temp_file_addr = download_url($_POST['update_url']);
				
				if ( is_wp_error($temp_file_addr) ) {
					
					$error = $temp_file_addr->get_error_code();
				
					if($error == 'http_no_url') {
					//The source file was not found or is invalid
						function woothemes_framework_update_missing_source_warning() {
							echo "<div id='source-warning' class='updated fade'><p>Failed: Invalid URL Provided</p></div>";
						}
						add_action('admin_notices', 'woothemes_framework_update_missing_source_warning');
					}
					
					return;

				  } 
				//Unzipp it
				$to = ABSPATH . 'wp-content/themes/' . get_option('template') . "/functions/";
				$dounzip = unzip_file($temp_file_addr, $to);
				
				unlink($temp_file_addr); // Delete
				
				if ( is_wp_error($dounzip) ) {
					
					$error = $dounzip->get_error_code();
									
					if($error == 'incompatible_archive') {
						//The source file was not found or is invalid
						function woothemes_framework_update_no_archive_warning() {
							echo "<div id='akismet-warning' class='updated fade'><p>Failed: Incompatible archive</p></div>";
						}
						add_action('admin_notices', 'woothemes_framework_update_no_archive_warning');
					}
					
					return;

				   } 
				 
				}
				function woothemes_framework_updated_success() {
							echo "<div id='framework-upgraded' class='updated fade'><p>Framework successfully downloaded and extracted. Your framework have been upgraded.</p></div>";
						}
						add_action('admin_notices', 'woothemes_framework_updated_success');
		
			}
		}                     
}

add_action('admin_head','woothemes_framework_update_head');
?>