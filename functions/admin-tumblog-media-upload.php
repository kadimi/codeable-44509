<?php 

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Ajax Handlers
	- Tumblog File Upload

-----------------------------------------------------------------------------------*/

require_once("../../../../wp-config.php");
require_once("../../../../wp-admin/includes/file.php");
require_once("../../../../wp-admin/admin-functions.php");

/*-----------------------------------------------------------------------------------*/
/* Tumblog */
/*-----------------------------------------------------------------------------------*/


tumblog_file_upload();



//Functionality
function tumblog_file_upload() {
	
	//wp_insert_attachment( $attachment, $filename, $parent_post_id );
	
	global $wpdb; // this is how you get access to the database
	//$themename = get_option('template') . "_";
	//Uploads
		
	$filename = $_FILES['userfile']; // [name] [tmp_name]
	$override['test_form'] = false;
	$override['action'] = 'wp_handle_upload';    
	//$jeff = 'test';
	
	$uploaded_file = wp_handle_upload($filename, $override); // [file] [url] [type]
	
	$attachment['post_title'] = $filename['name']; //post_title, post_content (the value for this key should be the empty string), post_status and post_mime_type
	$attachment['post_content'] = '';
	$attachment['post_status'] = 'inherit';
	$attachment['post_mime_type'] = $uploaded_file['type'];
	$attachment['guid'] = $uploaded_file['url'];
	
	$wud = wp_upload_dir(); // [path] [url] [subdir] [basedir] [baseurl] [error] 
	$filename_attach = $wud['basedir'].$uploaded_file['file'];
	
	$attach_id = wp_insert_attachment( $attachment, $filename_attach, 0 );
  	$attach_data = wp_generate_attachment_metadata( $attach_id, $filename_attach );
  	wp_update_attachment_metadata( $attach_id,  $attach_data );
	
	//$upload_tracking[] = $clickedID;
	//update_option( $clickedID , $uploaded_file['url'] );
	//update_option( $themename . $clickedID , $uploaded_file['url'] );
	if(!empty($uploaded_file['error'])) {echo 'Upload Error: ' . $uploaded_file['error']; }	
	else { echo $uploaded_file['url'].'|'.$attach_id; } // Is the Response

	
}

?>