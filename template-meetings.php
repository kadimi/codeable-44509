<?php
/*
Template Name: Mötesarena
*/
?>

<?php get_header(); ?>



		<!-- PAGE START -->

		<div class="container_12" id="main">

			<div class="grid_8" id="content">

				<div class="grid_8 area">

					<div class="text">

						<?php if (have_posts()) : $count = 0; ?>

							<?php while (have_posts()) : the_post(); $count++; ?>

								<?php the_content(); ?>

							<?php endwhile; else: ?>

								<div class="post">

								<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>

								</div><!-- /.post -->

						<?php endif; ?>

					</div>

				</div>

				<div class="clear"></div>

			</div>

<div id="sidebar">

<?php get_sidebar('meetings'); ?>

</div>

		</div>



		<!-- PAGE END -->



<?php get_footer(); ?>
