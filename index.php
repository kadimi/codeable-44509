<?php get_header(); ?>
	<div class="container_12" id="main">
		<div class="grid_12">
			<div class="grid_8 alpha">
				<div class="grid_8 slider">
					<?php $link= pods('startimage'); $link->find('sort_order ASC'); $total_links = $link->total_found(); $counter = 0; ?>
					<div id="loopedSlider" style="display:none;clear: both; margin: 0 auto; position: relative; width: 100%;">
						<div class="container" style="cursor: pointer; height: auto; overflow: hidden; position: relative; width: 100%;">
							<div class="slides" style="position: absolute; width: 2850px; top: 0px; left: 0px;">
								<?php while ( $link->fetch() ) : $title = $link->field('name'); $image = $link->field('image'); $url= $link->field('url'); if($total_links > 1 && $counter == $total_links -1 ) $counter = -1; ?>
								<div style="position: absolute; left: <?php echo $counter * 950?>px; display: block;">
									<a href="<?php echo $url?>"><img width="100%" height="320" src="<?php echo $image[0]['guid']?>"></a>
								</div>
								<?php $counter = $counter+1; endwhile ?>
							</div>
						</div>
					</div>
					<?php $link= pods('startimage'); $link->find('sort_order ASC'); $total_links = $link->total_found(); $counter = 0; ?>
					<div id="fadeSlider" style="clear: both; height: 320px; max-height: 320px !important; width: 100%; position:relative;overflow:hidden;">
						<?php while ( $link->fetch() ) : $title = $link->field('name'); $image = $link->field('image'); $url= $link->field('url'); if($total_links > 1 && $counter == $total_links -1 ) $counter = -1; ?>
						<a href="<?php echo $url?>"><img width="100%" height="320" src="<?php echo $image[0]['guid']?>"></a>
						<?php $counter = $counter+1; endwhile ?>
					</div>
				</div>
				<div class="grid_8 alpha" id="start-content">
					<!--
					<div class="grid_8 alpha">
						<div class="grid_8 alpha">
							<?php $home_page_id = get_option('woo_home_page_id'); ?> <?php if ($home_page_id && ($home_page_id <> "Select a page:")) : ?> <?php query_posts('page_id=' . get_page_id( $home_page_id )); ?> <?php if (have_posts()) : the_post(); ?>
							<div class="start-text">
								<?php the_content(); ?>
							</div>
							<?php endif; ?> <?php endif; ?>
						</div>
					</div>
					-->
					<div class="grid_8 startboxes large alpha">
						<?php
							$items = pods('startsplash');
							$items->find('sort_order ASC');
							$total_items = $items->total_found();

							$counter = 0;
							while ( $items->fetch()) :
								$title = $items->field('name');
								$image = $items->field('image');
								$text= $items->field('content');
								$url= $items->field('url');
								$baseurl = get_bloginfo('url');
								$url = $baseurl.$url;
								$counter++;
							if ( $counter != $total_items ) {
								echo '<a href="'.$url.'"><div class="box"><div class="frame">';
							} else {
								echo '<a href="'.$url.'"><div class="box last"><div class="frame">';
							}
							echo '<h3>'.$title.'</h3>';
							echo $text;
							echo '<img src="'.$image[0]['guid'].'" alt="" width="80" height="80">';
							echo '</div>';
							echo '</div></a>';
							endwhile;
						?>
					</div>
					<div class="grid_8 startboxes alpha">
						<?php
							$items = pods('startlink');
							$items->find('sort_order ASC');
							$total_items = $items->total_found();

							while ( $items->fetch()) :
								$title = $items->field('name');
								$image = $items->field('image');
								$text= $items->field('content');
								$url= $items->field('url');
								$baseurl = get_bloginfo('url');
								$url = $baseurl.$url;
								echo '<a href="'.$url.'"><div class="box"><div class="frame">';
								echo '<img src="'.$image[0]['guid'].'" class="right" alt="" width="80" height="80">';
								echo '<h3>'.$title.'</h3>';
								echo $text;
								echo '</div>';
								echo '</div></a>';
							endwhile;
						?>
						<div class="box last">
							<div class="frame">
								<h3>Følg oss!</h3>
								<p>Bli med oss på sosiale medier. Da vil du finne ut alt, akkurat som det skjer.</p>
								<div class="social-icons">
									<a href="https://www.facebook.com/kaffeknappennorge"><img class="first" src="<?php echo get_bloginfo('template_directory')?>/images/icons/facebook.png" alt=""></a>
									<a href="https://instagram.com/kaffeknappen_no/"><img src="<?php echo get_bloginfo('template_directory')?>/images/icons/instagram.png" alt=""></a>
									<a href="https://www.linkedin.com/company/840828"><img src="<?php echo get_bloginfo('template_directory')?>/images/icons/linkedin.png" alt=""></a>
									<a href="http://scae.com"><img src="<?php echo get_bloginfo('template_directory')?>/images/icons/speciality-coffee.png" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="sidebar">
				<div class="contact-sidebar">
					<img src="<?php echo get_bloginfo('template_directory')?>/images/contact-person.png" alt="">
					<h2>Vil du også ha en<span>ny pauseløsning på kontoret?</span></h2>
					<p>Fyll ut nedenfor, så hører du fra oss!</p>
					<!--
					<form action="">
						<div class="radio-group">
							<label><input type="radio" name="city" value="Stockholm"> Stockholm</label>
							<label><input type="radio" name="city" value="Uppsala"> Uppsala</label>
							<label><input type="radio" name="city" value="Göteborg"> Göteborg</label> <br>
							<label><input type="radio" name="city" value="Västerås"> Västerås</label>
							<label><input type="radio" name="city" value="Annan stad"> Annan stad</label>
						</div>
						<div class="text-fields">
							<input type="text" name="name" placeholder="Namn">	
							<input type="text" name="company" placeholder="Företag">	
							<input type="text" name="phone" placeholder="Telefon">	
							<input type="text" name="e-mail" placeholder="E-post">
							<input type="submit" class="submit-btn" value="Skicka >>">
						</div>
					</form>
					-->
					<?php echo do_shortcode( '[contact-form-7 id="3274" title="Kontaktformulär 1"]' ); ?>
				</div>
				<div class="barista-sidebar">
					<div class="barista-logo">
						<img src="<?php echo get_bloginfo('template_directory')?>/images/barista-logo.png" alt="">
					</div>
					<?php wp_reset_query(); ?>
	 				<?php query_posts($query_string . '&cat=-14,-15'); ?>
					<?php if (have_posts()) : $count = 0; ?> <?php while (have_posts() && $count < 3) : the_post(); $count++; ?>
						<?php echo '<div class="barista-article last-article">';?>
						<h3><?php the_title(); ?></h3>
						<!--
						<div class="barista-img">
							<?php $thumbnail = get_post_meta($post -> ID, 'barista_thumbnail', true);
							      if (!empty($thumbnail)) {
							          echo '<a href="'.get_permalink().'"><img class="bw-img"src="'.$thumbnail[0]['image'].'" alt="'.esc_attr($thumbnail[0]['title']).'" width="260" height="85" /></a>';
							      }
	      					?>
						</div>
						-->
						<span><?php the_time('d M, Y'); ?>&nbsp;</span>
						<p>
							<?php echo excerpt(13); ?>
							<a class="read-more" href="<?php the_permalink() ?>">Les mer</a>
						</p>
				</div>
				<?php endwhile;?>
				<?php endif; ?>
				<a class="archive-link" href="/barista_corner">Nyhetsarkiv <span>>></span></a>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
