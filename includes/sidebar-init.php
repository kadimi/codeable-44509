<?php



// Register widgetized areas



function the_widgets_init() {

    if ( !function_exists('register_sidebars') )

        return;



    register_sidebar(array('name' => 'Sidebar Blog','id' => 'sidebar-1','description' => 'This sidebar will only appear on blog page template','before_widget' => '<div id="%1$s" class="widget %2$s"><div class="box"><div class="frame">','after_widget' => '</div><div class="b"></div></div></div>','before_title' => '<h3>','after_title' => '</h3>'));    

    register_sidebar(array('name' => 'Sidebar Home','id' => 'sidebar-2','description' => 'This sidebar will only appear on homepage','before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));    

    register_sidebar(array('name' => 'Sidebar Page','id' => 'sidebar-3','description' => 'This sidebar will only appear on pages','before_widget' => '<div class="box"><div class="frame">','after_widget' => '</div><div class="b"></div></div>','before_title' => '<h3>','after_title' => '</h3>'));    

    register_sidebar(array('name' => 'Sidebar Meetingarea','id' => 'sidebar-5','description' => 'This sidebar will only appear on meeting area page','before_widget' => '<div class="box"><div class="frame">','after_widget' => '</div><div class="b"></div></div>','before_title' => '<h3>','after_title' => '</h3>'));    

    register_sidebar(array('name' => 'Sidebar Product','id' => 'sidebar-6','description' => 'This sidebar will only appear on product pages','before_widget' => '<div class="box"><div class="frame">','after_widget' => '</div><div class="b"></div></div>','before_title' => '<h3>','after_title' => '</h3>'));    

    register_sidebar(array('name' => 'Footer 1','id' => 'footer-1','description' => 'This widgetized area will appear in left footer','before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));

    register_sidebar(array('name' => 'Footer 2','id' => 'footer-2','description' => 'This widgetized area will appear in footer','before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));

    register_sidebar(array('name' => 'Footer 3','id' => 'footer-3','description' => 'This widgetized area will appear in footer','before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));

    register_sidebar(array('name' => 'Footer 4','id' => 'footer-4','description' => 'This widgetized area will appear in footer','before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));

    register_sidebar(array('name' => 'Footer 5','id' => 'footer-5','description' => 'This widgetized area will appear in footer','before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>'));

}



add_action( 'init', 'the_widgets_init' );





    

?>