<?php

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Content link widget
-----------------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------------*/
/* Content link Widget */
/*---------------------------------------------------------------------------------*/

class KK_ContentWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('description' => 'Use this widget to add any type of Ad as a widget.' );
		parent::__construct(false, __('KK - Content link', 'woothemes'),$widget_ops);
	}

	function widget($args, $instance) {
		$title = $instance['title'];
		$image = $instance['image'];
		$href = $instance['href'];

        echo '<div class="contentlink-widget widget">';

		if($title != '')
			echo '<h3>'.$title.'</h3>';

		if($image != '')
echo '<img src="'.$image.'" class="right" alt="" width="97" height="84">';

		if($href != '')
echo '<a href="'.$href.'" class="more">Les mer</a>';

		echo '</div>';

	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form($instance) {
		$title = esc_attr($instance['title']);
		$image = esc_attr($instance['image']);
		$href = esc_attr($instance['href']);
		?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','woothemes'); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('image'); ?>"><?php _e('Image Url:','woothemes'); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('image'); ?>" value="<?php echo $image; ?>" class="widefat" id="<?php echo $this->get_field_id('image'); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('href'); ?>"><?php _e('Link URL:','woothemes'); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('href'); ?>" value="<?php echo $href; ?>" class="widefat" id="<?php echo $this->get_field_id('href'); ?>" />
        </p>
        <?php
	}
}

register_widget('KK_ContentWidget');

/*---------------------------------------------------------------------------------*/
/* Splash links Widget */
/*---------------------------------------------------------------------------------*/

class KK_SplashLinksWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('description' => 'Use this widget to display splashes-links to other pages.' );
		parent::__construct(false, __('KK - Splash links', 'woothemes'),$widget_ops);
	}

	function widget($args, $instance) {
          $link= pods('splash');
          $link->find('RAND()');
          $total_links = $link->total_found();

$post_id = $GLOBALS['post']->ID;

$counter = 0;

while ( $link->fetch() && $counter<2 ) :


		$title = $link->field('name');
		$image = $link->field('image');
		$text= $link->field('text');
		$url= $link->field('url');
		$button = $link->field('button_image');
		$extraclass = '';
		$exclude = $link->field('exclude');

		if($button == 'Send') {$extraclass = 'send';}

if($exclude != $post_id) {
$counter++;
echo '<div class="box"><div class="frame">';
echo '<img src="'.$image[0]['guid'].'" class="right" alt="" width="97" height="84">';
echo '<h3>'.$title.'</h3>';
echo '<p>'.$text.'</p>';
echo '<a href="'.$url.'" class="more '.$extraclass.'">Les mer</a>';
echo '</div>';
echo '<div class="b"></div></div>';
}
endwhile;

	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form($instance) {

	}
}

register_widget('KK_SplashLinksWidget');

/*---------------------------------------------------------------------------------*/
/* Categories Widget */
/*---------------------------------------------------------------------------------*/

class KK_CategoriesWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('description' => 'Use this widget to display categories.' );
		parent::__construct(false, __('KK - Category links', 'woothemes'),$widget_ops);
	}

	function widget($args, $instance) {

$title = $instance['title'];

	$categories = get_categories('exclude=14,15');
	echo '<div class="box"><div class="frame">';
	echo '<h3>'.$title.'</h3>';

	echo '<ul class="links-list">';

	foreach ($categories as $cat) {
		$li  = '<li><a href="'.get_category_link($cat->cat_ID).'"><span>'.$cat->cat_name.'</span></a></li>';
		echo $li;
	}

	echo '</ul>';

	echo '</div><div class="b"></div></div>';

	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form($instance) {
		$title = esc_attr($instance['title']);
		?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','woothemes'); ?></label>
            <input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
        </p>
        <?php
	}
}

register_widget('KK_CategoriesWidget');

?>
