<?php 

/*-----------------------------------------------------------------------------------

TABLE OF CONTENTS

- Page Navigation
- Misc
- Integrated plugins

-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/
/* PAGE NAVIGATION */
/*-----------------------------------------------------------------------------------*/
function woo_pagenav() { 

	if (function_exists('wp_pagenavi') ) { ?>
    
		<?php wp_pagenavi(); ?>
    
	<?php } else { ?>    
    
        <div class="more_entries">
            <div class="fl"><?php previous_posts_link(__('&laquo; Newer Entries ', 'woothemes')) ?></div>
            <div class="fr"><?php next_posts_link(__(' Older Entries &raquo;', 'woothemes')) ?></div>
            <br class="fix" />
        </div>	
    
	<?php }   
}                	

/*-----------------------------------------------------------------------------------*/
/* MISC */
/*-----------------------------------------------------------------------------------*/

// Remove image dimensions from woo_get_image images 
update_option('woo_force_all',false);
update_option('woo_force_single',false);
update_option('woo_updater',true);


/*-----------------------------------------------------------------------------------*/
/* INTEGRATED PLUGINS */
/*-----------------------------------------------------------------------------------*/

require_once ($includes_path . 'theme-plugins.php');
    
    
?>