<?php

function woo_options(){
// VARIABLES
$themename = "Delegate";
$manualurl = 'http://www.woothemes.com/support/theme-documentation/delegate';
$shortname = "woo";

$GLOBALS['template_path'] = get_bloginfo('template_directory');

//Access the WordPress Categories via an Array
$woo_categories = array();  
$woo_categories_obj = get_categories('hide_empty=0');
foreach ($woo_categories_obj as $woo_cat) {
    $woo_categories[$woo_cat->cat_ID] = $woo_cat->cat_name;}
$categories_tmp = array_unshift($woo_categories, "Select a category:");    
       
//Access the WordPress Pages via an Array
$woo_pages = array();
$woo_pages_obj = get_pages('sort_column=post_parent,menu_order');    
foreach ($woo_pages_obj as $woo_page) {
    $woo_pages[$woo_page->ID] = $woo_page->post_name; }
$woo_pages_tmp = array_unshift($woo_pages, "Select a page:");       


//Testing 
$options_select = array("one","two","three","four","five"); 
$options_radio = array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five"); 

//Stylesheets Reader
$alt_stylesheet_path = TEMPLATEPATH . '/styles/';
$alt_stylesheets = array();

if ( is_dir($alt_stylesheet_path) ) {
    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) { 
        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) {
            if(stristr($alt_stylesheet_file, ".css") !== false) {
                $alt_stylesheets[] = $alt_stylesheet_file;
            }
        }    
    }
}

//More Options
$all_uploads_path = site_url('wp-content/uploads/');
$all_uploads = get_option('woo_uploads');
$other_entries = array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");

// THIS IS THE DIFFERENT FIELDS
$options = array();   

$options[] = array( "name" => "General Settings",
                    "type" => "heading");
                        
$options[] = array( "name" => "Theme Stylesheet",
					"desc" => "Select your themes alternative color scheme.",
					"id" => $shortname."_alt_stylesheet",
					"std" => "default.css",
					"type" => "select",
					"options" => $alt_stylesheets);

$options[] = array( "name" => "Custom Logo",
					"desc" => "Upload a logo for your theme, or specify the image address of your online logo. (http://yoursite.com/logo.png)",
					"id" => $shortname."_logo",
					"std" => "",
					"type" => "upload");    
                                                                                     
$options[] = array( "name" => "Text Title",
					"desc" => "Enable if you want Blog Title and Tagline to be text-based. Setup title/tagline in WP -> Settings -> General.",
					"id" => $shortname."_texttitle",
					"std" => "false",
					"type" => "checkbox");

$options[] = array( "name" => "Custom Favicon",
					"desc" => "Upload a 16px x 16px Png/Gif image that will represent your website's favicon.",
					"id" => $shortname."_custom_favicon",
					"std" => "",
					"type" => "upload"); 
                                               
$options[] = array( "name" => "Tracking Code",
					"desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
					"id" => $shortname."_google_analytics",
					"std" => "",
					"type" => "textarea");        

$options[] = array( "name" => "RSS URL",
					"desc" => "Enter your preferred RSS URL. (Feedburner or other)",
					"id" => $shortname."_feedburner_url",
					"std" => "",
					"type" => "text");
                    
$options[] = array( "name" => "Contact Form E-Mail",
					"desc" => "Enter your E-mail address to use on the Contact Form Page Template.",
					"id" => $shortname."_contactform_email",
					"std" => "",
					"type" => "text");

$options[] = array( "name" => "Date Format",
					"desc" => "Date format to display in posts using <a href=\"http://www.php.net/date/\">PHP date parameters</a>.",
					"id" => $shortname."_date",
					"std" => "d. M, Y",
					"type" => "text");

$options[] = array( "name" => "Custom CSS",
                    "desc" => "Quickly add some CSS to your theme by adding it to this block.",
                    "id" => $shortname."_custom_css",
                    "std" => "",
                    "type" => "textarea");

$options[] = array( "name" => "Layout Options",
					"type" => "heading");    

$options[] = array( "name" => "Show Breadcrumbs",
					"desc" => "Show breadcrumb at the top of every page, post and archives.",
					"id" => $shortname."_breadcrumbs",
					"std" => "false",
					"type" => "checkbox");   

$options[] = array( "name" => "Show Excerpt in Blog",
					"desc" => "Check this to show the excerpt instead of the content in the blog template.",
					"id" => $shortname."_excerpt",
					"std" => "false",
					"type" => "checkbox");   

$options[] = array( "name" => "Contact info - Name",
					"desc" => "Show your contact info in top right corner of website.",
					"id" => $shortname."_address_name",
					"std" => "",
					"type" => "text"); 

$options[] = array( "name" => "Contact info - Address",
					"desc" => "Show your contant info in top right corner of website.",
					"id" => $shortname."_address_street",
					"std" => "",
					"type" => "text"); 

$options[] = array( "name" => "Contact info - Phone",
					"desc" => "Show your contant info in top right corner of website.",
					"id" => $shortname."_address_phone",
					"std" => "",
					"type" => "text"); 

$options[] = array( "name" => "Contact info - Email",
					"desc" => "Show your contant info in top right corner of website.",
					"id" => $shortname."_address_email",
					"std" => "",
					"type" => "text"); 

$options[] = array( "name" => "Styling Options",
					"type" => "heading");    

$options[] = array( "name" =>  "Link Color",
					"desc" => "Pick a custom color for links or add a hex color code e.g. #697e09",
					"id" => "woo_link_color",
					"std" => "",
					"type" => "color");   

$options[] = array( "name" =>  "Link Hover Color",
					"desc" => "Pick a custom color for links hover or add a hex color code e.g. #697e09",
					"id" => "woo_link_hover_color",
					"std" => "",
					"type" => "color");                                       

$options[] = array( "name" =>  "Button Color",
					"desc" => "Pick a custom color for buttons or add a hex color code e.g. #697e09",
					"id" => "woo_button_color",
					"std" => "",
					"type" => "color");                                       

$options[] = array( "name" => "Navigation",
					"type" => "heading");    

$options[] = array( "name" => "Category Navigation",
					"desc" => "Swap the Page navigation for a Category navigation. ",
					"id" => $shortname."_cat_menu",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Exclude Pages or Categories from Navigation",
					"desc" => "Enter a comma-separated list of <a href='http://support.wordpress.com/pages/8/'>ID's</a> that you'd like to exclude from the top navigation. (e.g. 12,23,27,44)",
					"id" => $shortname."_nav_exclude",
					"std" => "",
					"type" => "text"); 

$options[] = array(	"name" => "Featured Slider",
					"type" => "heading");					

$options[] = array( "name" => "Disable Slider",
					"desc" => "Check this to disable the slider on the homepage.",
					"id" => $shortname."_slider_disable",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array(	"name" => "Featured Slider Pages",
					"desc" => "Enter a comma-separated list of the <a href='http://faq.wordpress.com/2008/05/29/how-to-find-page-id-numbers/'>page ID's</a> that you'd like to display in the featured slider. (ie. 1,2,3,4)",
					"id" => $shortname."_feat_pages",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Exclude From Navigation",
					"desc" => "Check this box if you wish to exclude the slider pages from the main navigation.",
					"id" => $shortname."_ex_feat_pages",
					"std" => "true",
					"type" => "checkbox");	
                    
$options[] = array(    "name" => "Animation Speed",
                    "desc" => "The time in <b>seconds</b> the animation between frames will take.",
                    "id" => $shortname."_slider_speed",
                    "std" => 0.6,
                    "type" => "text");

$options[] = array(    "name" => "Auto Start",
                    "desc" => "Set the slider to start sliding automatically.",
                    "id" => $shortname."_slider_auto",
                    "std" => "false",
                    "type" => "checkbox");   
                    
$options[] = array(    "name" => "Auto Slide Interval",
                    "desc" => "The time in <b>seconds</b> each slide pauses for, before sliding to the next.",
                    "id" => $shortname."_slider_interval",
                    "std" => 4.0,
                    "type" => "text");

$options[] = array( "name" => "Homepage",
					"type" => "heading");    

$options[] = array(	"name" => "Homepage Content (below slider)",
					"desc" => "Select a page that you would like to show on the front page below the slider.",
					"id" => $shortname."_home_page_id",
					"std" => "Select a page:",
					"type" => "select",
					"options" => $woo_pages);     

$options[] = array( "name" => "Show Blog on Homepage",
                    "desc" => "Check this if you want to show normal blog posts under the slider. Set number of posts in WP Settings > Reading.",
                    "id" => $shortname."_home_blog",
                    "std" => "false",
                    "type" => "checkbox");   
                   
$options[] = array( "name" => "Show Full Content",
                    "desc" => "Check this if you want to show the full content of blog posts instead of the excerpt.",
                    "id" => $shortname."_home_blog_content",
                    "std" => "false",
                    "type" => "checkbox");   

$options[] = array( "name" => "Dynamic Images",
				    "type" => "heading");    

$options[] = array( "name" => "Enable Dynamic Image Resizer",
					"desc" => "This will enable the thumb.php script. It dynamically resizes images on your site.",
					"id" => $shortname."_resize",
					"std" => "true",
					"type" => "checkbox");    
                    
$options[] = array( "name" => "Automatic Image Thumbs",
					"desc" => "Enable this to use the default WP uploader with thumbnails. The first uploaded image will be used, if there is no 'image' uploaded in custom fields.",
					"id" => $shortname."_auto_img",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Show Image in RSS feed",
					"desc" => "Will show the image uploaded to 'image' custom field in the RSS feed.",
					"id" => $shortname."_rss_thumb",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Image Dimensions",
					"desc" => "Enter an integer value i.e. 250 for the desired size which will be used when dynamically creating the images.",
					"id" => $shortname."_image_dimensions",
					"std" => "",
					"type" => array( 
									array(  'id' => $shortname. '_thumb_w',
											'type' => 'text',
											'std' => 100,
											'meta' => 'Width'),
									array(  'id' => $shortname. '_thumb_h',
											'type' => 'text',
											'std' => 100,
											'meta' => 'Height')
								  ));

//Advertising
$options[] = array( "name" => "Ads - Top Ad (468x60px)",
                    "type" => "heading");

$options[] = array( "name" => "Enable Ad",
					"desc" => "Enable the ad space",
					"id" => $shortname."_ad_top",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Adsense code",
					"desc" => "Enter your adsense code (or other ad network code) here.",
					"id" => $shortname."_ad_top_adsense",
					"std" => "",
					"type" => "textarea");

$options[] = array( "name" => "Image Location",
					"desc" => "Enter the URL to the banner ad image location.",
					"id" => $shortname."_ad_top_image",
					"std" => "http://www.woothemes.com/ads/woothemes-468x60-2.gif",
					"type" => "upload");
					
$options[] = array( "name" => "Destination URL",
					"desc" => "Enter the URL where this banner ad points to.",
					"id" => $shortname."_ad_top_url",
					"std" => "http://www.woothemes.com",
					"type" => "text");                        
                                               

update_option('woo_template',$options);      
update_option('woo_themename',$themename);   
update_option('woo_shortname',$shortname);
update_option('woo_manual',$manualurl);

                                     
// Woo Metabox Options
                    

$woo_metaboxes = array(
        "image" => array (
            "name" => "image",
            "label" => "Image",
            "type" => "upload",
            "desc" => "Upload file here..."
        )
    );
    
update_option('woo_custom_template',$woo_metaboxes);      

/*
function woo_update_options(){
        $options = get_option('woo_template',$options);  
        foreach ($options as $option){
            update_option($option['id'],$option['std']);
        }   
}

function woo_add_options(){
        $options = get_option('woo_template',$options);  
        foreach ($options as $option){
            update_option($option['id'],$option['std']);
        }   
}


//add_action('switch_theme', 'woo_update_options'); 
if(get_option('template') == 'wooframework'){       
    woo_add_options();
} // end function 
*/


}

add_action('init','woo_options');  

?>