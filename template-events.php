<?php
/*
Template Name: Kalendarium (lista)
*/

?>
<?php get_header(); ?>
       
    <div id="content" class="col-full">
		<div id="main" class="col-left box">
		
			<?php if ( get_option( 'woo_breadcrumbs' ) == 'true') { yoast_breadcrumb('<div id="breadcrumb"><p>','</p></div>'); } ?>
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div <?php post_class(); ?>>

                    <h1 class="title"><?php the_title(); ?></h1>
                    
                    <div class="entry">
	                	<?php the_content(); ?>


        <?php
          $event= pods('event');
          $event->find('name ASC');
          $total_events = $event->total_found();
        ?>

        <?php if( $total_events >0 ) : ?>
<div class="event-list">  
          <?php while ( $event->fetch() ) : ?>

            <?php
              // set our variables
              $event_name      = $event->field('name');
           ?>
<div class="post entry">  
<h2 class="title"><?php echo $event->field('name'); ?></h2>
                    
                    <p class="post-meta">
                        <span>När:&nbsp;</span><span class="date"><?php echo date('Y-m-d',strtotime($event->field('when'))); ?> •</span>
                        <span>Var:&nbsp;<?php echo $event->field('where'); ?></span>
                    </p>
                    
                    <div class="entry"><p><?php echo $event->field('beskrivning'); ?></p>
                    </div><!-- /.entry -->
    
                </div>



            <!-- /member -->

          <?php endwhile ?>
</div>
        <?php endif ?>



	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                
                <?php if ('open' == $post->comment_status) : ?>
	                <?php comments_template(); ?>
				<?php endif; ?>
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->

        <?php get_sidebar('page'); ?>

    </div><!-- /#content -->
		
<?php get_footer(); ?>