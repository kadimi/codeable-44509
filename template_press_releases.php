<?php
/*
Template Name: Press / Dokument
*/
?>
<?php get_header(); ?>
       
		<!-- PAGE START -->
		<div class="container_12" id="main">
			<div class="grid_8" id="content">
				<div class="grid_8 area">
					<div class="text">
						<?php if (have_posts()) : $count = 0; ?>
							<?php while (have_posts()) : the_post(); $count++; ?>
								<?php the_content(); ?>
							<?php endwhile; else: ?>
								<div class="post">
								<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
								</div><!-- /.post -->
						<?php endif; ?>  

        				<?php
          					$items= pods('pressrelease');
          					$items->find('name ASC');
          					$total_items = $items->total_found();
        				?>
        				<?php while ( $items->fetch() ) : 
        				
        				$title = $items->field('name'); 
        				$text = $items->field('content'); 
        				$file = $items->field('attachement'); 
        				$filetype = $items->field('doc_type');
        				$fileicon = 'logos_pdf.png';

if($filetype == 'Word') {$fileicon='logos_word.png';} 
if($filetype == 'PDF') {$fileicon='logos_pdf.png';} 

        				?>
						<div style="" class="press_item">
							<div style="" class="image">
								<img height="100" width="100" alt="<?php echo $filetype; ?>" src="<?php bloginfo('template_directory'); ?>/images/<?php echo $fileicon;?>">
							</div>
							<div class="content" style="width:500px;">
								<h3><?php echo $title; ?></h3>
								<p><?php echo $text; ?></p>
								<a href="<?php echo $file[0]['guid']?>">Ladda ner</a>
							</div>
						</div>
						<?php endwhile ?>
					</div>				
				</div>
				<div class="clear"></div>
			</div>
<div id="sidebar">
<?php get_sidebar('page'); ?>
</div>
		</div>

		<!-- PAGE END -->
		
<?php get_footer(); ?>