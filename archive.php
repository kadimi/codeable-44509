<?php get_header(); ?>

		<div class="container_12" id="main">
			<div class="grid_8" id="content">
				<div  class="grid_7 alpha">
					<?php if (is_category()) { ?>
					<h1 class="archive_header"><span class="fl cat"><?php _e('Arkiv', 'woothemes'); ?> - <?php echo single_cat_title(); ?></h1>

					<?php } elseif (is_day()) { ?>
					<h1 class="archive_header"><?php _e('Archive', 'woothemes'); ?> - <?php the_time($GLOBALS['woodate']); ?></h1 >

					<?php } elseif (is_month()) { ?>
					<h1 class="archive_header"><?php _e('Arkiv', 'woothemes'); ?> - <?php the_time('F, Y'); ?></h1>

					<?php } elseif (is_year()) { ?>
					<h1 class="archive_header"><?php _e('Archive', 'woothemes'); ?> - <?php the_time('Y'); ?></h1>

					<?php } elseif (is_author()) { ?>
					<h1 class="archive_header"><?php _e('Archive by Author', 'woothemes'); ?></h1>

					<?php } elseif (is_tag()) { ?>
					<h1 class="archive_header"><?php _e('Tag Archives:', 'woothemes'); ?> <?php echo single_tag_title('', true); ?></h1>

					<?php } ?>
				</div>

 <?php query_posts($query_string . '&cat=-14,-15'); ?>
				<?php if (have_posts()) : $count = 0; ?>
					<?php while (have_posts()) : the_post(); $count++; ?>

				<div class="clear"></div>
				<div class="grid_8 area">
					<div class="box">
						<strong><?php the_time('H:i'); ?></strong>
						<strong><?php the_time('d M'); ?></strong>
						<?php the_time('Y'); ?>
					</div>
					<div class="text">
						<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
						<?php the_content(); ?>
					</div>
				</div>

					<?php endwhile; else: ?>
				<div class="clear"></div>
				<div class="grid_8 area">
					<div class="text">
						<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div id="sidebar">
				<?php dynamic_sidebar('sidebar-1'); ?>
			</div>
		</div>
<?php get_footer(); ?>
