<?php

/*-----------------------------------------------------------------------------------*/
/* Start WooThemes Functions - Please refrain from editing this section */
/*-----------------------------------------------------------------------------------*/

// Set path to WooFramework and theme specific functions
$functions_path = TEMPLATEPATH . '/functions/';
$includes_path = TEMPLATEPATH . '/includes/';

// WooFramework
require_once ($functions_path . 'admin-init.php');			// Framework Init

// Theme specific functionality
require_once ($includes_path . 'theme-options.php'); 		// Options panel settings and custom settings
require_once ($includes_path . 'theme-functions.php'); 		// Custom theme functions
require_once ($includes_path . 'theme-plugins.php');		// Theme specific plugins integrated in a theme
require_once ($includes_path . 'theme-actions.php');		// Theme actions & user defined hooks
require_once ($includes_path . 'theme-comments.php'); 		// Custom comments/pingback loop
require_once ($includes_path . 'theme-js.php');				// Load javascript in wp_head
require_once ($includes_path . 'sidebar-init.php');			// Initialize widgetized areas
require_once ($includes_path . 'theme-widgets.php');		// Theme widgets

/*-----------------------------------------------------------------------------------*/
/* End WooThemes Functions - You can add custom functions below */
/*-----------------------------------------------------------------------------------*/

require_once ($includes_path . 'kk-widgets.php');		// Theme widgets
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');


add_filter( 'getarchives_where', 'customarchives_where' );

function customarchives_where( $x ) {

	global $wpdb;

	$s = $x;

	$s =  $s . " AND $wpdb->posts.ID IN ";

	$s = $s . "(";
	$s = $s . "SELECT $wpdb->posts.ID FROM $wpdb->posts INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) WHERE $wpdb->term_taxonomy.taxonomy = 'category'";

	$exclude = '14, 15'; // category id or list of id's to exclude

	$s = $s . " AND $wpdb->term_taxonomy.term_id NOT IN ($exclude)";
	$s = $s . ")";

	return $s;

}

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}
 
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }	
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

function custom_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_editor_style('css/wysiwyg.css');

add_action( 'init', 'kadimi_fix_background_images' );
function kadimi_fix_background_images() {

  // Run only once
  $fixed = get_option( 'kadimi_fix_background_images' );
  if ( $fixed ) {
    return;
  } else {
    add_option( 'kadimi_fix_background_images', '1', null, 'yes' );
  }

  /**
   * Get affected rows.
   */
  global $wpdb;
  $results = $wpdb->get_results("
    SELECT *
    FROM {$wpdb->postmeta}
    WHERE 1 = 1
      AND `meta_key`   LIKE 'background_image'
      AND `meta_value` LIKE '%image%'
  ", ARRAY_A);

  /**
   * Fix.
   */
  foreach ($results as $index => $record) {
    $i = 1;
    while( $i < 300 ) {
      $results[ $index ][ 'meta_value' ] = preg_replace( '/"image";s:\d+/', '"image";s:' . $i , $record[ 'meta_value' ]);
      $unserialized = unserialize(  $results[ $index ][ 'meta_value' ] );
      $is_good = is_array( $unserialized);
      if ( $is_good ) {
        delete_post_meta( $record[ 'post_id' ], 'background_image' );
        add_post_meta( $record[ 'post_id' ], 'background_image', $unserialized );
        break;
      }
      $i++;
    }
  }

  /**
   * Another fix.
   */
  update_post_meta( 3, 'background_name', 'Hjem', 'Huem' );
}
